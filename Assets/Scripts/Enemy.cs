using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    Vector2 a;
    private float health;
    // Start is called before the first frame update
    void Start()
    {
        health = 100;
        a = new Vector2(0, transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        TargetTracking(a, 2f);

    }
    public void TargetTracking(Vector3 a, float velo)
    {
        gameObject.transform.position = Vector3.MoveTowards(transform.position, a, Time.deltaTime * velo);
    }
    public void OnHit(float damage)
    {
        Debug.Log("oof");
        health-= damage;
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
