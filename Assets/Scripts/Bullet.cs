using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Vector2 a;
    // Start is called before the first frame update
    void Start()
    {

        a = new Vector2(20, transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        TargetTracking(a, 4f);
        if (transform.position.x == 20)
        {
            Destroy(gameObject);
        }
    }
    public void TargetTracking(Vector2 a, float velo)
    {
        gameObject.transform.position = Vector3.MoveTowards(transform.position, a, Time.deltaTime * velo);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("hit");
        if(collision.gameObject.TryGetComponent<Enemy>(out Enemy enemy))
        {
            enemy.OnHit(50);
        }
        Destroy(gameObject);
    }
}
