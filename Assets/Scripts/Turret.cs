using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject Bullet;
    float firerate = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (firerate > 0)
        {
            firerate-=Time.deltaTime;
        }
        else
        {
            foreach (GameObject o in GameObject.FindGameObjectsWithTag("enemy"))
            {
                if (o.transform.position.y == gameObject.transform.position.y)
                {
                    Debug.Log("Fire");
                    Instantiate(Bullet, gameObject.transform.position, Bullet.transform.rotation);
                }
            }
            firerate = 1;
        }
    }
}
