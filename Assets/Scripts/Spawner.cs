using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject turret;
    public GameObject enemy;
    Point TopRight=new Point(0,10);
    Point Downleft= new Point(20,0);
    float timer=2;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(turret,new Vector2(TopRight.X,TopRight.Y),turret.transform.rotation);
        Instantiate(turret, new Vector2(TopRight.X, Downleft.Y), turret.transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
                Instantiate(enemy, new Vector2(Downleft.X, TopRight.Y), enemy.transform.rotation);
                Instantiate(enemy, new Vector2(Downleft.X, Downleft.Y), enemy.transform.rotation);
            
            timer = 2;
        }
    }
}
